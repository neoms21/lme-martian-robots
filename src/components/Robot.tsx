import React, { useState } from 'react';

import { Flex, Text } from '@chakra-ui/layout';
import ChakraNumberInput from './NumberInput';
import { Button, Input, Select } from '@chakra-ui/react';
import { Orientation } from '../types';
import { RobotInput } from './Home';

interface Props {
  onRobotAssigned: (robot: RobotInput) => void;
  id: string;
}

const Robot: React.FC<Props> = ({ onRobotAssigned, id }) => {
  const [robotX, setRobotX] = useState('');
  const [robotY, setRobotY] = useState('');
  const [orientation, setOrientation] = useState<number>(0);
  const [commands, setCommands] = useState('');

  return (
    <Flex marginTop={6} direction="column" justifyContent="flex-start" alignItems="flex-start">
      <Flex justifyContent="center" marginTop={4}>
        <ChakraNumberInput label="Robot X" onValueChange={setRobotX} />
        &nbsp;&nbsp;&nbsp;&nbsp;
        <ChakraNumberInput label="Robot Y" onValueChange={setRobotY} />
        <Flex marginLeft={4} direction="column" alignItems="flex-start">
          <Text>Orientation</Text>
          <Select onChange={(e) => setOrientation(Number.parseInt(e.target.value))}>
            <option value={Orientation.E}>E</option>
            <option value={Orientation.W}>W</option>
            <option value={Orientation.N}>N</option>
            <option value={Orientation.S}>S</option>
          </Select>
        </Flex>
        <Flex marginLeft={4} direction="column" alignItems="flex-start">
          <Text>Commands</Text>
          <Input onChange={(e) => setCommands(e.target.value)} size="md" />
        </Flex>
        <Button
          colorScheme="purple"
          disabled={!robotX || !robotY || orientation === undefined || !commands}
          alignSelf="flex-end"
          marginLeft={4}
          onClick={() => {
            console.log({
              id,
              x: Number.parseInt(robotX),
              y: Number.parseInt(robotY),
              orientation: orientation,
              commands,
              isValid: true,
            });
            onRobotAssigned({
              id,
              x: Number.parseInt(robotX),
              y: Number.parseInt(robotY),
              orientation: 0,
              commands,
              isValid: true,
            });
          }}
        >
          Set Robot
        </Button>
      </Flex>
    </Flex>
  );
};

export default Robot;
