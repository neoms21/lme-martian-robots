import React, { useState } from 'react';

import { Flex, Text } from '@chakra-ui/layout';
import {
  NumberDecrementStepper,
  NumberIncrementStepper,
  NumberInput,
  NumberInputField,
  NumberInputStepper,
} from '@chakra-ui/react';
const parse = (val: string) => val.replace(/^\$/, '');

interface Props {
  label: string;
  onValueChange: (value: string) => void;
}

const ChakraNumberInput: React.FC<Props> = ({ label, onValueChange }) => {
  const [value, setValue] = useState('');

  const handleOnChange = (valueString: string) => {
    const parsed = parse(valueString);
    setValue(parsed);
    onValueChange(parsed);
  };

  return (
    <Flex direction="column" alignItems="flex-start">
      <Text>{label}</Text>
      <NumberInput onChange={handleOnChange} value={value} max={50}>
        <NumberInputField />
        <NumberInputStepper>
          <NumberIncrementStepper />
          <NumberDecrementStepper />
        </NumberInputStepper>
      </NumberInput>
    </Flex>
  );
};

export default ChakraNumberInput;
