import React from 'react';
import styled from 'styled-components';

const StyledPoint = styled.div<{ isActive?: boolean, showCoordinates?:boolean }>`
  width: ${({ showCoordinates }) => (showCoordinates ? 'auto' : '12px')};
  height: ${({ showCoordinates }) => (showCoordinates ? 'auto' : '12px')};
  border-radius: 6px;
  color: white;
  background-color: ${({ isActive }) => (isActive ? 'green' : 'black')};
`;

interface Props {
  isActive: boolean;
  rowIndex: number;
  colIndex: number;
  showCoordinates?: boolean;
}

const GridPoint = React.forwardRef<HTMLDivElement, Props>(
  ({ rowIndex, colIndex, isActive, showCoordinates }, ref) => {
    return (
      <StyledPoint showCoordinates isActive={isActive} ref={ref}>
        {showCoordinates && (
          <span>
            {rowIndex}, {colIndex}
          </span>
        )}
      </StyledPoint>
    );
  }
);

export default GridPoint;
