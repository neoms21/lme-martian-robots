import React, { useState } from 'react';
import { Coordinates, GridInput } from '../types';
import Grid from './Grid';

interface PlaygroundProps {
  gridInputs: GridInput[];
}

const Playground: React.FC<PlaygroundProps> = ({ gridInputs }) => {
  const [activeGridIndexes, setActiveGridIndexes] = useState([0]);
  const [unsafePoints, setUnsafePoints] = useState<Coordinates[]>([]);

  const handleAnimationComplete = (unsafeCoordinates: Coordinates | undefined, indexToAdd: number) => {
    if (unsafeCoordinates) {
      setUnsafePoints([...unsafePoints, unsafeCoordinates]);
    }
    setActiveGridIndexes([...activeGridIndexes, indexToAdd]);
  };

  return (
    <div>
      {gridInputs.map((gridInput, index) =>
        activeGridIndexes.includes(index) ? (
          <Grid
            key={index}
            unsafePoints={unsafePoints}
            gridInput={gridInput}
            onAnimationComplete={(coordinates) => handleAnimationComplete(coordinates, index + 1)}
          />
        ) : null
      )}
    </div>
  );
};

interface PlaygroundProps {}

export default Playground;
