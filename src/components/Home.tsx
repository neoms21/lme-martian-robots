import { useState } from 'react';

import { Box, Flex, Heading } from '@chakra-ui/layout';
import ChakraNumberInput from './NumberInput';
import { Button } from '@chakra-ui/react';
import Robot from './Robot';
import { Orientation, GridInput } from '../types';
import { uuid } from 'uuidv4';
import Playground from './Playground';

export interface RobotInput {
  id: string;
  x: number;
  y: number;
  orientation: Orientation;
  commands: string;
  isValid?: boolean;
}

const Home = () => {
  const [gridX, setGridX] = useState('');
  const [gridY, setGridY] = useState('');
  const [robots, setRobots] = useState<Record<string, RobotInput>>({});
  const [gridInputs, setGridInputs] = useState<GridInput[]>([]);

  const handleRobotAssigned = (robot: RobotInput) => {
    setRobots({ ...robots, [robot.id]: robot });
  };

  const handleRobotAdd = () => {
    const id = uuid();
    setRobots({ ...robots, [id]: { id, x: 0, y: 0, orientation: Orientation.E, commands: '' } });
  };

  const handleStartModelling = () => {
    const validRobots = Object.values(robots).filter((r) => r.isValid);

    setGridInputs(
      validRobots.map((r) => ({
        coordinates: { x: Number.parseInt(gridX), y: Number.parseInt(gridY) },
        robot: {
          coordinates: { x: r.x, y: r.y },
          orientation: r.orientation,
        },
        commands: r.commands,
      }))
    );
  };

  const isModellingDisabled = !gridX || !gridY || Object.values(robots).filter((r) => r.isValid).length === 0;

  return gridInputs.length > 0 ? (
    <Playground gridInputs={gridInputs} />
  ) : (
    <Box>
      <Flex marginTop={6} direction="column" justifyContent="flex-start" alignItems="flex-start">
        <Heading as="h6" size="md">
          Grid Coordinates:
        </Heading>

        <Flex marginTop={4}>
          <ChakraNumberInput label="Grid X" onValueChange={setGridX} />
          &nbsp;&nbsp;&nbsp;&nbsp;
          <ChakraNumberInput label="Grid Y" onValueChange={setGridY} />
        </Flex>

        <Heading marginTop={4} as="h6" size="md">
          Robots:
        </Heading>
        <Button marginTop={2} onClick={handleRobotAdd}>
          Add Robots
        </Button>
        {Object.keys(robots).map((key) => (
          <Robot id={key} key={key} onRobotAssigned={handleRobotAssigned} />
        ))}
        <Button
          colorScheme="whatsapp"
          disabled={isModellingDisabled}
          marginTop={6}
          alignSelf="flex-start"
          onClick={handleStartModelling}
        >
          Start Modelling
        </Button>
      </Flex>
    </Box>
  );
};

export default Home;
