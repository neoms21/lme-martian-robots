import React, { useEffect, useRef, useState } from 'react';
import GridPoint from './GridPoint';

import rover from '../rover.svg';
import { Robot, Coordinates, GridInput, Orientation } from '../types';
import { calculatePosition, delay } from '../utils';
import { GridContainer, GridRow, Rover } from './Grid.styles';
import { parseIntoCommands, getEnumKeyByEnumValue } from '../utils/index';

interface GridProps {
  gridInput: GridInput;
  unsafePoints: Coordinates[];
  onAnimationComplete: (coordinates?: Coordinates) => void;
}

const Grid: React.FC<GridProps> = ({
  gridInput: { coordinates, robot, commands },
  onAnimationComplete,
  unsafePoints,
}) => {
  const { x, y } = coordinates;
  const commandsArr = parseIntoCommands(commands);
  const rows = new Array(x + 1).fill(1);
  const columns = new Array(y + 1).fill(1);

  const [localRobot, setLocalRobot] = useState<Robot>(robot);
  const [robotLocation, setRobotLocation] = useState<Coordinates>({ x: 0, y: 0 });
  const [result, setResult] = useState('');

  const robotRef = useRef(robot);
  const gridPoints = useRef<any>({});

  useEffect(() => {
    robotRef.current = localRobot;
  }, [localRobot]);

  useEffect(() => {
    const startAnimation = async () => {
      for (let i = 0; i < commands.length; i++) {
        const updatedRobot = calculatePosition(robotRef.current, commandsArr[i], { x, y }, unsafePoints);

        setLocalRobot(updatedRobot);
        if (updatedRobot.lost) {
          setResult(
            `${robotRef.current.coordinates.x} ${robotRef.current.coordinates.y} ${getEnumKeyByEnumValue(
              Orientation,
              robotRef.current.orientation
            )} LOST`
          );
          onAnimationComplete(updatedRobot.coordinates);
          return;
        }
        const newLoc = gridPoints.current[`${updatedRobot.coordinates.x}-${updatedRobot.coordinates.y}`];

        setRobotLocation({
          x: newLoc.offsetLeft,
          y: newLoc.offsetTop,
        });
        await delay();
      }
      setResult(
        `${robotRef.current.coordinates.x} ${robotRef.current.coordinates.y} ${getEnumKeyByEnumValue(
          Orientation,
          robotRef.current.orientation
        )}`
      );
      onAnimationComplete();
    };

    if (gridPoints.current) {
      const initialLocation = gridPoints.current[`${robot.coordinates.x}-${robot.coordinates.y}`];
      setRobotLocation({
        x: initialLocation.offsetLeft,
        y: initialLocation.offsetTop,
      });
    }
    startAnimation();
  }, []);

  return (
    <>
      <GridContainer rowCount={rows.length} columnCount={columns.length}>
        {columns.map((_c, colIndex) => (
          <GridRow key={`col-${colIndex}`}>
            {rows.map((_r, rowIndex) => (
              <GridPoint
                key={`row-${rowIndex}`}
                showCoordinates
                rowIndex={rowIndex}
                colIndex={colIndex}
                isActive={rowIndex === robot.coordinates.x && colIndex === robot.coordinates.y}
                ref={(el) => {
                  gridPoints.current[`${rowIndex}-${colIndex}`] = el;
                }}
              />
            ))}
          </GridRow>
        ))}

        <Rover location={robotLocation} src={rover} alt="rover" />
      </GridContainer>
      <span>{result}</span>
    </>
  );
};

export default Grid;
