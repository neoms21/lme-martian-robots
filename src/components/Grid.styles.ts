import styled from 'styled-components';

import { Coordinates } from '../types';

export const GridContainer = styled.div<{
  rowCount: number;
  columnCount: number;
}>`
  display: flex;
  flex-direction: column-reverse;
  /* width: 400px; */
  min-height: 200px;
  position: relative;
  justify-content: space-between;
  margin-bottom: 2rem;
  border: solid 1px black;
  padding: 1rem;
  margin-bottom: 1rem;
  margin-top: 1.5rem;
`;

export const Rover = styled.img<{
  location: Coordinates;
}>`
  height: 30px;
  width: 30px;
  position: absolute;
  left: ${({ location }) => `${location.x}px`};
  top: ${({ location }) => `${location.y}px`};
  transition: transform 0.5s linear;
`;

export const GridRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

// ${({ robot }) =>
// robot.move &&
// css`
//   transform: ${() => {
//     if (robot.direction === Direction.HORIZONTAL) {
//       return `translateX(${robot.movement * (robot.distance || 0)}px)`;
//     }
//     return `translateY(${robot.movement * (robot.distance || 0)}px)`;
//   }};
// `};
