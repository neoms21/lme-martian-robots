import { Command, NextDirection, Orientation, Robot, Coordinates, Direction } from '../types';

export const getEnumKeyByEnumValue = (myEnum: any, enumValue: number | string): string => {
  let keys = Object.keys(myEnum).filter((x) => myEnum[x] === enumValue);
  return keys.length > 0 ? keys[0] : '';
};

const getNewCoordinates = (orientation: Orientation, existingCoordinates: Coordinates): Robot => {
  switch (orientation) {
    case Orientation.E:
      return {
        coordinates: { ...existingCoordinates, x: existingCoordinates.x + 1 },
        
        orientation,
      };
    case Orientation.W:
      return {
        coordinates: { ...existingCoordinates, x: existingCoordinates.x - 1 },
        
        orientation,
      };
    case Orientation.N:
      return {
        coordinates: { ...existingCoordinates, y: existingCoordinates.y + 1 },
        orientation,
      };
    case Orientation.S:
      return {
        coordinates: { ...existingCoordinates, y: existingCoordinates.y - 1 },
        orientation,
      };

    default:
      break;
  }

  return {
    coordinates: existingCoordinates,
    
    orientation,
  };
};

const areCoordinatesUnsafe = (unsafeCoordinates: Coordinates[], coordinates: Coordinates): boolean =>
  unsafeCoordinates.find((u) => u.x === coordinates.x && u.y === coordinates.y) !== undefined;

export const delay = (durationInMiliseconds = 300) =>
  new Promise((resolve) =>
    setTimeout(() => {
      resolve(true);
    }, durationInMiliseconds)
  );

export const calculatePosition = (
  robot: Robot,
  command: Command,
  maxBounds: Coordinates,
  unsafePoints: Coordinates[]
): Robot => {
  const newRobot = { ...robot, direction: Direction.HORIZONTAL };
  if (command !== Command.F) {
    newRobot.orientation = NextDirection[robot.orientation][command];
  } else {
    const position = getNewCoordinates(robot.orientation, robot.coordinates);
    const unsafeLocation = areCoordinatesUnsafe(unsafePoints, position.coordinates);
    newRobot.coordinates = unsafeLocation ? robot.coordinates : position.coordinates;
    newRobot.lost = unsafeLocation
      ? false
      : position.coordinates.x > maxBounds.x || position.coordinates.y > maxBounds.y;
  }

  return newRobot;
};

export const parseIntoCommands = (value: string): Command[] => {
  return value.split('').map((c) => (<any>Command)[c]);
};

export const getFinalLocation = (
  robot: Robot,
  commands: Command[],
  maxBounds: Coordinates,
  unsafePoints: Coordinates[]
): string => {
  let updatedRobot = { ...robot };
  for (let i = 0; i < commands.length; i++) {
    updatedRobot = calculatePosition(updatedRobot, commands[i], maxBounds, unsafePoints);
    if (updatedRobot.coordinates.x > maxBounds.x || updatedRobot.coordinates.y > maxBounds.y)
      return `${updatedRobot.coordinates.x} ${updatedRobot.coordinates.y} ${getEnumKeyByEnumValue(
        Orientation,
        updatedRobot.orientation
      )} LOST`;
  }
  return `${updatedRobot.coordinates.x} ${updatedRobot.coordinates.y} ${getEnumKeyByEnumValue(
    Orientation,
    updatedRobot.orientation
  )}`;
};
