import { getFinalLocation } from '.';
import { Command, Direction, Orientation } from '../types';

describe('Utils test', () => {
  const getRobot = (x: number, y: number, orientation: Orientation) => ({
    coordinates: { x, y },
    orientation,
    movement: 1,
    direction: Direction.HORIZONTAL,
  });

  it('should return the final location', () => {
    const tests = [
      {
        x: 1,
        y: 1,
        orientation: Orientation.E,
        result: '1 1 E',
        commands: 'RFRFRFRF',
        maxBounds: { x: 5, y: 3 },
      },
      {
        x: 3,
        y: 3,
        orientation: Orientation.N,
        result: '3 4 N LOST',
        commands: 'FRRFLLFFRRFLL',
        maxBounds: { x: 5, y: 3 },
      },
      {
        x: 0,
        y: 3,
        orientation: Orientation.W,
        result: '3 4 N LOST',
        commands: 'LLFFFLFLFL',
        maxBounds: { x: 5, y: 3 },
      },
      {
        x: 3,
        y: 3,
        orientation: Orientation.N,
        result: '3 1 N',
        commands: 'RRFLLFFRRFLL',
        maxBounds: { x: 5, y: 3 },
        unsafePoints: [{ x: 3, y: 3 }],
      },
    ];

    tests.forEach(({ x, y, orientation, commands, result, maxBounds, unsafePoints }) => {
      const output = getFinalLocation(
        getRobot(x, y, orientation),
        commands.split('').map((c) => (<any>Command)[c]),
        maxBounds,
        unsafePoints || []
      );

      expect(output).toEqual(result);
    });
  });
});
