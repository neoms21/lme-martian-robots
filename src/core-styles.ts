import styled, { createGlobalStyle } from "styled-components";

export const GlobalStyles = createGlobalStyle`
  body {
    margin:0;
  }
`;

export const MainContent = styled.div`
  max-width: 1280px;
  margin: 0 auto;
  margin-top: 1rem;
  padding:24px;
`;
