import './App.css';
import { MainContent } from './core-styles';
import Home from './components/Home';

function App() {
  return (
    <div className="App">
      <header className="App-header">Martian Robots</header>
      <MainContent>
        <Home />
      </MainContent>
    </div>
  );
}

export default App;
