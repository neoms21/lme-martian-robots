export enum Orientation {
  W = 180,
  E = 0,
  N = -90,
  S = 90,
}

export enum Command {
  L = 'l',
  R = 'r',
  F = 'f',
}

export const NextDirection = {
  [Orientation.E]: { [Command.L]: Orientation.N, [Command.R]: Orientation.S },
  [Orientation.N]: { [Command.L]: Orientation.W, [Command.R]: Orientation.E },
  [Orientation.W]: { [Command.L]: Orientation.S, [Command.R]: Orientation.N },
  [Orientation.S]: { [Command.L]: Orientation.E, [Command.R]: Orientation.W },
};

export interface Coordinates {
  x: number;
  y: number;
}

export enum Direction {
  HORIZONTAL = 'h',
  VERTICAL = 'v',
}

export interface Robot {
  coordinates: Coordinates;
  orientation: Orientation;
  lost?: boolean;
}

export interface GridInput {
  coordinates: Coordinates;
  robot: Robot;
  commands: string;
}
