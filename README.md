# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

To install the dependencies
### `yarn`


In the project directory, you can run:
### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.


### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.


## Improvements

1. Validation messages on the inputs page <br/>
    a. validate robot's initial coordinates are within grid's<br/>
    b. Rogue commands are validated/ignored<br/>
2. Use of react-router to have two different pages for inputs and playground <br/>
3. Smooth animations on the grid using framer or react-spring
4. Tests for inputs page
5. Rotate the robot in case of orientation change


